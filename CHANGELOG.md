## 2022.02.09
* Fixed problem with audio not working since last kernel update
* Added Pinephone Keyboard kernel driver (so working keyboard)
* Improve our BTRFS scheme for Pinephone
* Kernel updated to 5.16.1
* Phosh updated to 0.15.0
* Phoc updated to 0.12.0

## 2021.04.30
* Our images are now using Btrfs with snapper! As Btrfs is a more robust filesystem, especially when compared to ext4, the long-term user experience should be improved. Additianlly, snapper adds a way to fall back (`snapper rollback`) in the case of broken updates - this should be possible from a chroot enviroment or from the recovery shell.
* Fixed: not being able to change the Plasma Mobile user password.
* Fixed: themes not showing in the Maliit Keyboard settings (Plasma Settings).

## 2021.04.18

### Main changes

As the changes are many and significant, users are recommended to _reinstall from scratch_, rather than updating.

* Added SD card automount udev rules to Plasma Mobile.
* Cleaned up patterns and KIWI files to avoid installing unnecessary packages.
* Updated Alsa UCM files.
* Improved boot times.
* PackageKit now is able to work offline, from Discover.
* Switched to eg25-manager over modem-power.
* Removed the sudoers file.
* Now A-GPS data is pushed to the modem every boot or every 12 hours, allowing the modem to get a fix under 10
  seconds instead of 12 minutes.
* The camera and the autofocus are now working.
* We have switched to Plymouth for the boot animation.
* Our first boot script has been mildly optimized.
* Pulseadio 14 no longer requires patching to make audio routing work.
* There is no X server in our Phosh images anymore, Plasma Mobile still requires XWayland to load.
* The firewall now comes enabled by default and allows KDE Connect on public networks, but only on the Plasma Mobile image.
* The AppArmor pattern is now installed by default in our images.
* Vibration feedback now works on Plasma Mobile, through hfd-service. Programs using it right now are, for
  example, Kalk and the Maliit virtual keyboard. Also feedbackd udev rules were added, so vibration works
  now on Phosh too.
* Added a upower patch to make the Phosh "torch" shortcut work.
* There were some bugfixes in the zypp PackageKit backend that should make the user experience better, see
  [boo#1180597](https://bugzilla.opensuse.org/show_bug.cgi?id=1180597).
* The modem still resets randomly.
* Portfolio is now our default file browser in Phosh.
* King's Cross required additional fonts that are now included. We also tossed in postmarketOS Tweaks to
  enhance the user experience in Phosh.
* Added some fonts in the Plasma Mobile image that make the Maliit keyboard and QMLKonsole look better.
* Not being able to send SMS with Chatty was fixed, see [sr#885811](https://build.opensuse.org/request/show/885863). This should, hopefully, not take too long to land after this release.
* Squeekboard was blocking the shutdown in Phosh, this was fixed by [updating to the version 1.13.0](https://source.puri.sm/Librem5/squeekboard/-/issues/274).
* **LOTS of bugfixes and updates**

Since we are a rolling distro, of course there were more changes that can't be put in a changelog, it would be too long!

Don't forget to [check our wiki](https://en.opensuse.org/HCL:PinePhone) for additional information!

### Known issues: 
* Annoying modem resets: developers are [working on this](https://invent.kde.org/teams/plasma-mobile/issues/-/issues/3) right now.
* Plasma Mobile can't make calls: this happens because the Dialer app is loaded too quickly and some Telepathy dependencies are not ready. Workaround is to `killall plasmaphonedialer` and open it again. The issue is similar to [what was fixed here](https://invent.kde.org/plasma-mobile/plasma-dialer/-/commit/2bfb2eff160e717b3d791ebeee843080640f0cba).
* Opening the recents menu in Plasma Mobile while another app is loading disables the home and close buttons.
* The lockscreen in Plama Mobile doesn't go away until the phone is unlocked for the first time.

### What's next?
* We want to try using Btrfs as the root filesystem so users can have the advantages of [snapper](https://en.opensuse.org/Portal:Snapper). They should be able to rollback from a *chroot* enviroment or the [Jumpdrive](https://github.com/dreemurrs-embedded/Jumpdrive) *telnet* terminal. This would not include the bootloader, so we still need to be very careful with it. Btrfs is known to be flash friendly, so we should have no issues with this once we can sort out the quirks.
* We need to start moving our packages from the PinePhone repository to the main openSUSE repositories, so other users can have access to the same software, in true *convergence* fashion.
* We may consider enabling **zram** by default.

### Other thoughts:
* Currently no MTP implementation is provided, we are considering [cmtp-responder](https://github.com/cmtp-responder/cmtp-responder) and [uMTP-Responder](https://github.com/viveris/uMTP-Responder), but, as of right now, there is no option to disable the services other than using *systemd* commands or directly *killing* them.
* Anbox is in our sight, but the kernel needs some patches to make it work, and we are hesitant to patch the kernel more than needed, since the end goal is to *mainline* everything. The fact that it needs XWayland to work doesn't help either, as we are trying to get rid of XOrg dependencies, aiming for a pure Wayland experience.
* The A-GPS script may be dropped in favor of [integration from eg25-manager](https://gitlab.com/mobian1/devices/eg25-manager/-/merge_requests/15#9a7acd08eb3028f21e697cc7a21bc17fcaea6b5d). The script, systemd unit and timer will be kept [in GitHub](https://gist.github.com/surprized/bbf733c4ebde8125d523501d153b5a71).
* Yes, we know we don't provide "Full disk" encryption, and with the PinePhone hardware limitations we can't really consider it. At one point we will have to implement it so we have the software base ready for the next *mainline* Linux smartphone. The I/O penalty makes it a tough choice.


## WIP

Working auto-rotate and update to latest packages

**Implemented enhancements:**

- Update PHOSH to 0.8.0
- Update PHOC to 0.6.0
- Update Feedbackd
- Use UUID for root disk

**Working:**
- Calls (Auto-switch audio profiles)
- SMS
- Mobile Data
- WiFi
- Front Camera (laggy)
- Back Camera (laggy)
- Touchscreen
- Brightness
- Sound & Mic
- Volume keys
- Proximity sensor
- Jack
- Bluetooth
- Auto-rotate

## 2020.12.22

**Implemented enhancements:**

- Add PackageKit to get a working app store in Pinephone
- Update to kernel 5.10.0
- Change WYS for Callaudiod to better performance in Calls
- Use LABEL for root disk
- Use Nautilus instead nemo

**Working:**
- Calls (Auto-switch audio profiles)
- SMS
- Mobile Data
- WiFi
- Front Camera (laggy)
- Back Camera (laggy)
- Touchscreen
- Brightness
- Sound & Mic
- Volume keys
- Proximity sensor
- Jack
- Bluetooth


**Not Working:**
- Accelerometer
- GPS

## 2020.12.16

Fix problems in modem and wireless

**Implemented enhancements:**

Installed libqmi-tools for modem
Update to Phosh 0.7.0
Update Phoc to 0.5.1
Update u-boot to work with Jumpdrive
Update Gnome packages to 3.38

**Working:**
- Calls (Auto-switch audio profiles)
- SMS
- Mobile Data
- WiFi
- Front Camera (laggy)
- Back Camera (laggy)
- Touchscreen
- Brightness
- Sound & Mic
- Volume keys
- Proximity sensor
- Jack
- Bluetooth


**Not Working:**
- Accelerometer
- GPS

## 2020.11.16

Add bootsplash for boot and shutdown

**Implemented enhancements:**

- Add bootsplash
- Fix kernel start (laggy bug)
- Fix audio problems in calls
- Added gnome-initial-setup to add language to your on-screen keyboard
- Fix problem changing between on-screen keyboard and emoji keyboard
- Change Pinhole for Megapixels for camera app
- Patched kernel with fixes for brightness
- Fix problem with modem in last release
- Now bluetooth are working correctly
- Update to squeekboard 1.11.0


**Working:**
- Calls (Auto-switch audio profiles)
- SMS
- Mobile Data
- WiFi
- Front Camera (laggy)
- Back Camera (laggy)
- Touchscreen
- Brightness
- Sound & Mic
- Volume keys
- Proximity sensor
- Jack
- Bluetooth


**Not Working:**
- Accelerometer
- GPS

## 2020.11.15

Firmware packages for bluetooth and autofocus on camera.

**Implemented enhancements:**

- Change Pinhole for Megapixels for camera app
- Patched kernel with fixes for brightness
- Fix problem with modem in last release
- Now bluetooth are working correctly
- Updated PHOSH to 0.5.1
- Updated PHOC to 0.4.4


**Working:**
- Calls (Auto-switch audio profiles)
- SMS
- Mobile Data
- WiFi
- Front Camera (laggy)
- Back Camera (laggy)
- Touchscreen
- Brightness
- Sound & Mic
- Volume keys
- Proximity sensor
- Jack
- Bluetooth


**Not Working:**
- Accelerometer
- GPS

## 2020.10.14

New packages with responsive UI and new keyboard

**Implemented enhancements:**

- Updated PHOSH to 0.4.5 (Fixed volume bar in quick settings)
- Updated PHOC to 0.4.3
- Updated Squeekboard to 1.9.3 (now with terminal controls)
- Adapted gnome-software to work in small screens (a.k.a gnome-software-phosh)
- Added Flashlight app (a.k.a Torch)
- Adapted u-boot to work with openSUSE standard scheme
- New Kernel 5.8 compiled in OBS (openSUSE build service)
- Now openSUSE image works with initramfs
- Added Nemo as default filemanager
- Updated pinephone-helpers with new udev rules


**Working:**
- Calls (Auto-switch audio profiles)
- SMS
- Mobile Data
- WiFi
- Front Camera (laggy)
- Back Camera (laggy)
- Touchscreen
- Brightness
- Sound & Mic
- Volume keys
- Proximity sensor
- Jack

**Not Working:**
- Bluetooth
- Accelerometer
- GPS

## 2020.10.05

Fix some problem with new pipeline (now we are using OBS from openSUSE)

**Implemented enhancements:**

- Update PHOSH to 0.4.4
- Update feedbackd to latest commit
- Update purple-mms-sms package to 0.1.8
- Update Tumbleweed to 20201004

**Working:**
- Calls (Auto-switch audio profiles)
- SMS
- Mobile Data
- WiFi
- Front Camera (laggy)
- Back Camera (laggy)
- Touchscreen
- Brightness
- Sound & Mic
- Volume keys
- Proximity sensor

**Not Working:**
- Bluetooth
- Accelerometer
- Jack
## 2020.09.24

Fix some problem with new pipeline (now we are using OBS from openSUSE)

**Implemented enhancements:**

- New auto-resize FS in first boot
- New fonts for terminal and system
- Upgraded UI packages

**Working:**
- Calls (Auto-switch audio profiles)
- SMS
- Mobile Data
- WiFi
- Front Camera (laggy)
- Back Camera (laggy)
- Touchscreen
- Brightness
- Sound & Mic
- Volume keys
- Proximity sensor

**Not Working:**
- Bluetooth
- Accelerometer
- Jack


## v0.0.2-3 (31 Aug 2020)

New packages and working camera app.

**Implemented enhancements:**

- Installed new Camera package
- Update gnome-shell to 3.36.5
- Update kernel to improve battery time
- Update Phosh to 0.4.3
- Update Phoc to 0.4.2

**Working:**
- Calls (Auto-switch audio profiles)
- SMS
- Mobile Data
- WiFi
- Front Camera (laggy)
- Back Camera (laggy)
- Touchscreen
- Brightness
- Sound & Mic
- Volume keys
- Proximity sensor

**Not Working:**
- Bluetooth
- Accelerometer
- Jack

## v0.0.2-2 (28 Jul 2020)

New packages and basic apps.

**Implemented enhancements:**

- New Gnome Control Center with patches for Mobile
- New Gnome Contacts
- New Gnome Usage with patches to fit
- Added gtherm package
- Improvement on suspend tweaks
- Update Calls app with patches for audio
- Update Phosh to 0.4.2
- Update Phoc to 0.4.1
- Update Wys to user as a user service
- Change dtsi to set audio-card-name as Pinephone
- Update Feedbackd theme

**Working:**
- Calls (Auto-switch audio profiles)
- SMS
- Mobile Data
- WiFi
- Touchscreen
- Brightness
- Sound & Mic
- Volume keys
- Proximity sensor

**Not Working:**
- Camera
- Bluetooth
- Accelerometer

## v0.0.2-1 (23 Jul 2020)

Upgrade UI base packages

**Implemented enhancements:**

- Lock kernel packages for allow "zypper dup" without breaking system
- Upgrade Phosh to version 0.4.0
- Upgrade Phoc to version 0.4.0
- Updated Modem setup
- Updated u-boot adding latest changes
- Updated Kernel-5.7.0 from last changes from Pine64
- Working proximity sensor

**Working:**
- Calls (Auto-switch audio profiles)
- SMS
- Mobile Data
- WiFi
- Touchscreen
- Brightness
- Sound & Mic
- Volume keys
- Proximity sensor

**Not Working:**
- Camera
- Bluetooth
- Accelerometer

## v0.0.1-8 (02 Jul 2020)

New images with better integration with Phosh and improved UI

**Implemented enhancements:**

- Change kernel to use new compiled kernel
- Modified boot to vibrate on start and load some colours in led before kernel loads
- Added change loglevel to see errors in boot
- Add chatty for working SMS

**Working:**
- Calls (Auto-switch audio profiles)
- SMS
- Mobile Data
- WiFi
- Touchscreen
- Brightness (not auto)
- Sound & Mic
- Volume keys

**Not Working:**
- Camera
- Bluetooth
- Accelerometer

## v0.0.1-7 (27 Jun 2020)

New images with better integration with Phosh and improved UI

**Implemented enhancements:**

- Added systemd service for auto-switch audio profiles with incoming calls
- Added pulseaudio service to integrate well with Phosh
- Added actkbd sofware to control volume keys
- Added libnotify-tools for scripting notifications on mobile

**Working:**
- Calls (Auto-switch audio profiles)
- Mobile Data
- WiFi
- Touchscreen
- Brightness (not auto)
- Sound & Mic
- Volume keys

**Not Working:**
- Camera
- Bluetooth
- Accelerometer

## v0.0.1-6 (25 Jun 2020)

New images with better integration with Phosh and improved UI

**Implemented enhancements:**

- Added some Sound feature
- Installed KGX Terminal instead Gnome-Terminal (fits better)
- Updated Phosh to 0.3.1
- Updated Phoc to 0.1.9
- Add new repository from openSUSE with GPG Key
- New Kernel 5.8

**Working:**
- Calls
- Mobile Data
- WiFi
- Touchscreen
- Brightness (not auto)
- Sound & Mic

**Not Working:**
- Camera
- Bluetooth
- Accelerometer

## v0.0.1-5 (22 Jun 2020)

New images with better integration with Phosh and improved UI

**Implemented enhancements:**

- Add gnome-keyring for WiFi password request
- Add ofono packages for SIM enhanced
- Add NetworkManager package
- Add pine user to sudoers file
- Create files for Alsa UCM (sun50i-a64-audi)
- Installed Firefox

**Working:**
- Mobile Data
- WiFi
- Touchscreen
- Brightness (not auto)

**Not Working:**
- Sound & Mic
- Camera
- Bluetooth

## v0.0.1-4 (18 Jun 2020)

Phosh interface for openSUSE

**Implemented enhancements:**

- Add Phosh and Phosh
- Add Arphic fonts

**Working:**
- WiFi (with some problems when asking for password)
- Brightness (not auto)
- Touchscreen

**Not Working:**
- Sound & Mic
- Camera
- Bluetooth


## v0.0.1-3 (17 Jun 2020)

First build with XFCE4

**Implemented enhancements:**

- Add XFCE4 as default UI
- Add ofono packages for SIM enhanced

**Working:**
- Touchscreen

**Not Working:**
- Sound & Mic
- Camera
- Bluetooth
- WiFi
- Mobile Data
- Brightness
